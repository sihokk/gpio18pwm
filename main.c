#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pigpiod_if2.h>

static void print_usage(const char *cmd)
{
    printf("Usage: %s { on | off | n }\n", cmd);
    printf("Options:    on    - turn on (set to 100%% duty cycle)\n");
    printf("            off   - turn off\n");
    printf("            n     - set duty cycle to n. 0 < n <= 1.0\n");
}

int main(int argc, char **argv)
{

    if (2 != argc)
    {
        print_usage(argv[0]);
        return EXIT_FAILURE;
    }

    float duty;

    if (0 == strcmp(argv[1], "on"))
    {
        duty = 1.0;
    }
    else if (0 == strcmp(argv[1], "off"))
    {
        duty = 0;
    }
    else
    {
        char *end = 0;
        duty = strtof(argv[1], &end);
        if (0 == end || argv[1] == end || duty <= 0 || duty > 1.0f)
        {
            print_usage(argv[0]);
            return EXIT_FAILURE;
        }
    }

    int conn = pigpio_start(0, 0);
    if (conn < 0)
    {
        printf("Connect failed. Make sure remote GPIO access (pigpiod daemon) is enabled\n");
        return EXIT_FAILURE;
    }

    printf("Connected to local pigpiod daemon by default port\n");

    const uint gpio = 18;

    int range = get_PWM_range(conn, gpio);
    int value = get_PWM_dutycycle(conn, gpio);

    printf("Current status: range = %d, value = %d\n", range, value);

    set_PWM_dutycycle(conn, gpio, (uint)(duty * range));

    value = get_PWM_dutycycle(conn, gpio);
    printf("Updated status: value = %d\n", value);

    pigpio_stop(conn);

    return 0;
}
